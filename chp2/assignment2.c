#define _USE_MATH_DEFINES

#include <math.h>
#include <stdio.h>

int main(void){
  float r = 10.0;
  float v = 4.0f/3.0f*M_PI*(r*r*r);
  printf("%f", v);
}