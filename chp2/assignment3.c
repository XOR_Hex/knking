#define _USE_MATH_DEFINES

#include <math.h>
#include <stdio.h>

int main(void){
  printf("Enter sphere's radius: ");

  float r;
  scanf("%f", &r);

  float v = 4.0f/3.0f*M_PI*(r*r*r);
  printf("Volume of sphere: %f\n", v);
}