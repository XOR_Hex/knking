#include <stdio.h>

int power(int base, int power){
    int return_val = base;
    for(int c = 1; c < power; c++){
        return_val = return_val * base;
    }
    return return_val;
}

int main(void){
    printf("\n3x^5 + 2x^4 - 5x^3 - x^2 + 7x - 6\n\n");
    printf("Enter a value for x: ");
    int x;
    scanf("%d", &x);
    printf("And the answer is: %d\n\n", (3*power(x,5) + 2*power(x,4) - 5*power(x,3) - power(x,2) + 7*x -6));
}