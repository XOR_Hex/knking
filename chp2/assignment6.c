#include <stdio.h>

int main(void){
    printf("\n((((3x +2)x - 5)x - 1)x +7)x -6\n\n");
    printf("Enter a value for x: ");
    int x;
    scanf("%d", &x);
    printf("And the answer is: %d\n\n", (((((3*x + 2) * x -5) * x - 1) * x +7) * x - 6));
}