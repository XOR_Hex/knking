#include <stdio.h>

int main(void){
    printf("\n\nEnter a dollar amount: ");
    int dollar;
    scanf("%d", &dollar);

    int twenties = dollar / 20;
    dollar = dollar - (twenties * 20);

    int tens = dollar / 10;
    dollar = dollar - (tens * 10);

    int fives = dollar / 5;
    dollar = dollar - (fives *5);

    printf("$20 bills: %d\n", twenties);
    printf("$10 bills: %d\n", tens);
    printf(" $5 bills: %d\n", fives);
    printf(" $1 bills: %d\n\n", dollar);
}