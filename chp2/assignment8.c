#include <stdio.h>

float cal_interest(float amount, float rate){
    float monthly_rate = (rate / 100) / 12;
    //printf("Interest: %.2f\n", amount * monthly_rate);
    return amount * monthly_rate;
}

int main(void){
    float loan, rate, payment;

    printf("\nEnter amount of loan: ");
    scanf("%f", &loan);

    printf("Enter interest rate: ");
    scanf("%f", &rate);

    printf("Enter monthly payment: ");
    scanf("%f", &payment);

    printf("\n");

    for(int x = 0;  x < 3; x++){
        loan = loan - payment + cal_interest(loan, rate);
        printf("Balance remaining after %d payment: $%.2f\n", x+1, loan);
    }
    return 0;
}