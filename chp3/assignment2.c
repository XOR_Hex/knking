#include <stdio.h>

int main(void){

    int item, day, month, year;
    float price;

    printf("Enter item number: ");
    scanf("%d", &item);

    printf("Enter unit price: ");
    scanf("%f", &price);

    printf("Enter purchase date (mm/dd/yyy): ");
    scanf("%d/%d/%d", &day, &month, &year);


    printf("Item\t\tUnit\t\tPurchase\n");
    printf("\t\tPrice\t\tDate\n");
    printf("%d\t\t$%7.2f\t%d/%d/%d\n\n", item, price, day, month, year);

    return 0;
}