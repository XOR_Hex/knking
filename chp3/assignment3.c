#include <stdio.h>

int main(void){

    int gsi_prefex, group_identifier, publisher_code, item_number, check_digit;

    printf("Enter ISBN: ");
    scanf("%d-%d-%d-%d-%d", &gsi_prefex, &group_identifier, &publisher_code, &item_number, &check_digit);

    printf("GSI prefix: %d\n", gsi_prefex);
    printf("Group identifier: %d\n", group_identifier);
    printf("Publisher code: %d\n", publisher_code);
    printf("Item Number: %d\n", item_number);
    printf("Check digit: %d\n", check_digit);

    return 0;
}