#include <stdio.h>

int main(void){

    int area_code, three_digit, four_digit;

    printf("Enter phone number [(xxx) xxx-xxxx]: ");
    scanf("(%d) %d-%d", &area_code, &three_digit, &four_digit);

    printf("You entered %d.%d.%d\n", area_code, three_digit, four_digit);

    return 0;
}