#include <stdio.h>

int main(void){
    int zero, one, two, three, four, five, six, seven, eight, nine, a, b, c, d, e, f;

    printf("Enter the numbers from 1 to 16 in any order:\n");
    scanf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", &zero, &one, &two, &three, &four, &five, &six, &seven, &eight, &nine, &a, &b, &c, &d, &e, &f);

    printf("%d %d %d %d\n", zero, one, two, three);
    printf("%d %d %d %d\n", four, five, six, seven);
    printf("%d %d %d %d\n", eight, nine, a, b);
    printf("%d %d %d %d\n", c, d, e, f);

    int row_one_sum = zero + one + two+ three;
    int row_two_sum = four + five + six + seven;
    int row_three_sum = eight + nine + a + b;
    int row_four_sum = c + d + e + f;

    printf("Row sums: %d %d %d %d\n", row_one_sum, row_two_sum, row_three_sum, row_four_sum);

    int column_one_sum = zero + four + eight + c;
    int column_two_sum = one + five + nine + d;
    int column_three_sum = two + six + a + e;
    int column_four_sum = three + seven + b + f;

    printf("Column Sums: %d %d %d %d\n", column_one_sum, column_two_sum, column_three_sum, column_four_sum);

    int backslash = zero + five + a + f;
    int forwardslash = c + nine + six + three;

    printf("Diagonal sums: %d %d\n", backslash, forwardslash);

    return 0;
}